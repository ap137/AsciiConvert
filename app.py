from flask import Flask, request, jsonify
import pandas as pd
import numpy

app = Flask(__name__)
DELIMITATIONAL_LETTER = 'h'


@app.route('/test',methods=['GET'])
def super_endpoint():
    if request.method == "GET":
        return jsonify({'response': 'Get request Called'})

@app.route('/convert', methods=['POST'])
def convert():
    req = request.form
    characters_list =  request.json['list']
    modified_list = apply_operation_to_list(convert_ascii_list_into_integers(characters_list))  
    return jsonify(modified_list), 200


def convert_ascii_list_into_integers(ascii_list):
    newlist = [ ord(x) for x in ascii_list ]
    return newlist

def apply_operation_to_list(ascii_int_list):
    updated_list = [ apply_operation(x) for x in ascii_int_list]
    return updated_list

def apply_operation(element):
    bounds = calculate_bounds()
    if(element >=97 and element < bounds[0]) or (element >=65 and element <bounds[1]):
        return element * 10
    else:
        return 0

def calculate_bounds():
    return (ord(DELIMITATIONAL_LETTER.upper()), ord(DELIMITATIONAL_LETTER.lower()))

if __name__=="__main__":
    app.run(debug=True)